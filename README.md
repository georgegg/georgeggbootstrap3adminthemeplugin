georgeggBootstrap3AdminThemePlugin
================================

Plugin  for Symfony 1.4 for Bootstrap 3

Installation
-----------

1. Enable plugin in `config/ProjectConfiguration.class.php`:
```
  public function setup()
  {
    $this->enablePlugins(
      [...],
      'georgeggBootstrap3AdminThemePlugin'
    );
    [...]
  }
```



2. Apply theme `apps/APPNAME/modules/MODULENAME/config/generator.yml`:

```
generator:
  param:
    theme:  bootstrap
```




3. Clear cache

```
$ php symfony cc
```




4. Generate modules and admin for modumes with theme Boostrap:

```
$ php symfony doctrine:generate-admin appname CLASSNAME --theme=bootstrap
```


5. (Optional) Use widget `georgeggWidgetFormBootstrapSelectDoubleList` :

```
$this->widgetSchema['clientes_list'] = new sfWidgetFormDoctrineChoice(array(
      'model' => 'CLASSNAME',
      'renderer_class' => 'georgeggWidgetFormBootstrapSelectDoubleList',
      'renderer_options' => array(
        'label_unassociated' => 'Unassociated',
        'label_associated' => 'Associated',
        ),
      ));
```
